import { Navigate } from "react-router-dom";
import { useEffect, useContext } from "react";
import Swal from 'sweetalert2'
import UserContext from "../UserContext";

export default function Logout(){
const { unsetUser, setUser } = useContext(UserContext);

unsetUser();

useEffect(() =>{
	setUser({
		id: null,
		isAdmin: null
	});
	Swal.fire({
	  icon: 'info',
	  title: 'Logged out!',
	  showConfirmButton: false,
	  timer: 1500
	})
});

return(
	<Navigate to="/login" />
)
}