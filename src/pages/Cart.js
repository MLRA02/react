import { useEffect, useState } from 'react';
import { Link } from "react-router-dom";
import { Container, Card,Row, Col, Button } from "react-bootstrap";
import { useNavigate } from 'react-router-dom';
import ayos from "../icons/ayos.png";

export default function Cart(){

    const[time,setTime] = useState(10)
    const location = useNavigate()

    useEffect(() => {
        let timerInterval = setTimeout(() => setTime(time-1), 1000)

        if(time===0){
            location("/");
        }

        return () => {
            clearTimeout(timerInterval)
        }

    })

  return (
    <Container className="main-container">
            <Row>
            <Col>
            <Card>
              <Row className="main-container">
                          <Col className="p-5 text-center">
                          	<img src={ayos} alt='' className="ayos p-5 pt-0"/>	
                              <h1 className="ff-bold" >404 - Page Under Construction!</h1>
                              <p>This page is under construction,We are working very hard to give you the best experience with this website.</p>
                              <p>
                              Redirecting you to the homepage in {time} seconds...                
                              </p>
                              <Button variant="dark" as = {Link} to="/">Go to Home Page</Button>
                          </Col>
                      </Row>
            </Card>
            </Col>
            </Row>
            </Container>
  )
}

