import { useEffect, useState } from "react";
import ProductCard from "../components/ProductCard";
import bannerP from "../images/bannerP.png";
import React from "react"
import { Link } from "react-router-dom";


import {Container, Row, Col, Button, Stack } from 'react-bootstrap';


export default function Products() {


	const [products, setProducts] = useState([]); 

	useEffect(() =>{
		fetch(`${process.env.REACT_APP_API_URL}/products/active`)
		.then(res => res.json())
		.then(data => {
			console.log(data);
			setProducts(data.map(product =>{
				return(
					<ProductCard key={product._id} productProp={product}/>
				);
			}));
		})
	}, []);
	return(

				<>
		<Row>
		<img src={bannerP} alt=""/>
		</Row>
		<Container className="cat-container my-5 pb-5">
			<Row>
				<Col xs={12} lg={4}>
				<Stack gap={3} className="col-md-5 mx-auto">
					<Button as={Link} to={"/products/running"} variant="dark" size="sm" style={{ backgroundColor: 'black', color: 'white',  borderRadius: '50px 15px',marginTop:'1%' }}>Running Shoes</Button>
				</Stack>
				</Col>
					

				<Col xs={12} lg={4}>
				<Stack gap={3} className="col-md-5 mx-auto">
					<Button as={Link} to={"/products/basketball"} variant="dark" size="sm" style={{ backgroundColor: 'black', color: 'white',  borderRadius: '50px 15px',marginTop:'1%' }}>Basketball Shoes</Button>
				</Stack>
				</Col>	

				<Col xs={12} lg={4}>
				<Stack gap={3} className="col-md-5 mx-auto">
					<Button as={Link} to={"/products/sneaker"} variant="dark" size="sm" style={{ backgroundColor: 'black', color: 'white',  borderRadius: '50px 15px',marginTop:'1%' }}>Sneakers</Button>
				</Stack>
				</Col>

			</Row>
		</Container>
		<Container className="main-container pt-0 p-5">
		<Row>
		  <h1 className="text-center ff-bold p-1">Products</h1>
		</Row>
		<Row className="products-row p-5">
		  {products.map((product, index) => (
		    <Col xs={12} lg={4} className="p-1" key={index}>
		      {product}
		    </Col>
		  ))}
		</Row>
		</Container>
		</>
	)
}