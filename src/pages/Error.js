import { useEffect, useState } from 'react';
import { Link } from "react-router-dom";
import { Row, Col, Button } from "react-bootstrap";
import { useNavigate } from 'react-router-dom';

export default function Error(){

    const[time,setTime] = useState(5)
    const location = useNavigate()

    useEffect(() => {
        let timerInterval = setTimeout(() => setTime(time-1), 1000)

        if(time===0){
            location("/");
        }

        return () => {
            clearTimeout(timerInterval)
        }

    })

    return(

        <Row className="main-container">
            <Col className="p-5 text-center">
                <h1 className="ff-bold" >404 - Page Not Found!</h1>
                <p>The page you are trying to access cannot be found.</p>
                <p>
                Redirecting you to the homepage in {time} seconds...                
                </p>
                <Button variant="dark" as = {Link} to="/">Back to Store</Button>
            </Col>
        </Row>
        
    )
    
}