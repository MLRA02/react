import { useEffect, useState, useContext } from "react";
import UserContext from "../UserContext";
import {Navigate} from "react-router-dom";
import {Container, Row, Col, Accordion, Stack } from 'react-bootstrap';
 import Sidebar from "../components/SidebarMenu.js";


export default function AllOrders() {


    const [orders, setOrders] = useState([]); 

    const { user } = useContext(UserContext);



    useEffect(() =>{
        fetch(`${process.env.REACT_APP_API_URL}/orders/all`, {
            headers:{
                "Authorization": `Bearer ${localStorage.getItem("token")}`
            }
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);
            setOrders(data.map(order =>{


                return(

                
            <Accordion defaultActiveKey="0">
            <Accordion.Item eventKey="0">
                <Accordion.Header>{order.purchasedOn}</Accordion.Header>
                <Accordion.Body>
                <Stack>
                User ID: {order.userId}
                    {
                        order.products.map((product) => {
                            return (
                                <>
                                  
                                <Stack>
                                <span><b>Order:</b></span>
                                <span>Product ID : {product.productId}</span>
                                <span> Quantity : {product.quantity}</span>
                                </Stack>
                                </>
                            )
                        })
                    }
                Total: {order.totalAmount}
                    </Stack>
                </Accordion.Body>
         </Accordion.Item>
          </Accordion>


                );
            }));
        })
    }, []);
    return(

        <>
        <Container fluid className="w-100 d-flex h-100 m-0 p-0">
        <Row className="w-100 m-0 p-0">
        <Col className="bg-white d-flex  pt-3 m-0 shadow" xs={12} md={2} lg={2}>
        <Sidebar />
        </Col>

        <Col className="colr-bg m-0 p-5 d-flex justify-content-center" xs={12} md={10} lg={10}>
        {
            (user.isAdmin)

        ?
        <>   

        <div className="w-100 bg-white  rounded shadow-sm shadow-lg p-5">
            <div className="my-2 text-center">
                <h1 className="ff-bold pb-5">VIEW ALL ORDERES</h1>
            </div>
            <div>
                <Container>
                <Row className="all-orders-row">
                {orders}
                </Row>
                </Container>
            </div>
        </div>
        </>

        :
        <Navigate to="/" />}

        </Col>
        
        </Row>
    </Container>
    </>
   
        
    )
}