import React from "react"
import { Link } from "react-router-dom";
import { Container, Row, Col, Button, Stack } from "react-bootstrap";
import Hero from "../components/Hero.js";
import runningImg from "../images/running.jpg"
import sneakersImg from "../images/sneakers.jpg"
import basketballImg from "../images/basketball.jpg"
import truckIcon from "../icons/truck.png"
import certificateIcon from "../icons/certificate.png"
import guaranteeIcon from "../icons/guarantee.png"
import fbIcon from "../icons/fb1.png";
import insta from "../icons/insta.png";
import twitter from "../icons/twitter.png";
import shoe from "../icons/shoe.png";
import sm from "../icons/sm.png";


export default function Home(){



	return(
		<>
		<Hero />
		

	{/*categories*/}
		<Container className="cat-container my-5 pb-5">
			<Row className="text-center">
			<h2 className="ff-bold pt-5"> SHOP BY CATEGORY</h2>
			</Row>
			<Row>
				<Col xs={12} lg={4}>
				<img src={runningImg} alt='' className="p-5"/>
				<Stack gap={3} className="col-md-5 mx-auto">
					<Button as={Link} to={"/products/running"} variant="dark" size="sm" style={{ backgroundColor: 'black', color: 'white',  borderRadius: '50px 15px' }}>Running Shoes</Button>
				</Stack>
				</Col>
					

				<Col xs={12} lg={4}>
				<img src={basketballImg} alt='' className="p-5"/>
				<Stack gap={3} className="col-md-5 mx-auto">
					<Button as={Link} to={"/products/basketball"} variant="dark" size="sm" style={{ backgroundColor: 'black', color: 'white',  borderRadius: '50px 15px' }}>Basketball Shoes</Button>
				</Stack>
				</Col>	

				<Col xs={12} lg={4}>
				<img src={sneakersImg} alt='' className="p-5"/>
				<Stack gap={3} className="col-md-5 mx-auto">
					<Button as={Link} to={"/products/sneaker"} variant="dark" size="sm" style={{ backgroundColor: 'black', color: 'white',  borderRadius: '50px 15px' }}>Sneakers</Button>
				</Stack>
				</Col>

			</Row>
		</Container>

	{/*promos*/}
	<Container className="promo-container text-center" fluid>
		<Row>
			<Col xs={12} md={4} className="promo-icon-col p-3">
				<Stack className="d-flex align-items-center p-2">
				<img src={truckIcon} alt='' />
				<h5 className="p-1 ff-bold">Free Shipping</h5>
				<p>Nationwide</p>
				</Stack>

			</Col>
			<Col xs={12} md={4} className="promo-icon-col p-3">
				<Stack className="d-flex align-items-center p-2">
				<img src={certificateIcon} alt=''/>
				<h5 className="p-1 ff-bold">Certified Original</h5>
				<p>The product'ss is certified original, guaranteeing its authenticity and quality.</p>
				</Stack>
			</Col>
			<Col xs={12} md={4} className="promo-icon-col p-3">
				<Stack className="d-flex align-items-center p-2">
				<img src={guaranteeIcon} alt=''/>
				<h5 className="p-1 ff-bold">100% Satisfaction</h5>
				<p>"The product's high-quality materials and exceptional craftsmanship make it well worth the investment."</p>
				</Stack>
			</Col>
		</Row>
	</Container>

	{/*history*/}

	<Container className="main-container sale-container">
		<Row>
			<Col xs={12} md={6} className="sale-txt">
				<Stack>
				 <h1 className="sale-title ff-bold">Shoes Made in Marikina</h1>	
				 <p>Marikina City, located in the Philippines, is known for its long history of shoe-making dating back to the late 18th century. The industry was introduced by Spanish colonizers, who taught the locals how to make shoes using traditional European techniques. The city quickly became a hub for shoe production and by the early 20th century, it was known as the "Shoe Capital of the Philippines." Marikina's shoe-making industry thrived for many years and was responsible for providing employment for many of the city's residents. However, like many traditional industries, it faced challenges in the form of increased competition from imported shoes and changing consumer preferences. Nevertheless, Marikina shoe-making remains an important part of the city's history and culture, and many local manufacturers continue to produce high-quality shoes using traditional techniques.
				 </p>
				</Stack>
			</Col>
			<Col className="sale-img">
			</Col>
		</Row>
	</Container>

{/*contact us*/}

	<Container className="contact-container" fluid>
		<Container className="contact1-container" fluid>
		<Row className="pt-3  ff-reg" >
			
			<Col xs={12} md={4}  >
			<Stack>
			<Col className="ff-bold"> Visit our social media sites: </Col>
			<a className="social" href="https://www.facebook.com/"><img src={fbIcon} alt=""/> Facebook</a>
			<a className="social" href="https://www.instagram.com/"><img src={insta} alt=""/>Instagram </a>
			<a className="social" href="https://twitter.com/"><img src={twitter} alt=""/>Twitter   </a>
			</Stack>
			</Col>
			<Col xs={12} md={4}  >
			<p className="ff-bold" style={{ marginBottom: '0' }}>Contact us through:</p>
			<p style={{ marginBottom: '0' }}>shoemadeinmarikina@mail.com</p>
			<p style={{ marginBottom: '0' }}> +639123456789 </p>
			<p style={{ marginBottom: '0' }}> <img src={shoe} alt=""/> </p>
			</Col>
			<Col xs={12} md={4}  >
			<p className="ff-bold" style={{ marginBottom: '0' }}>Visit Our Store!</p>
			<p style={{ marginBottom: '0' }}> #123 Marikina City </p>
			<p style={{ marginBottom: '0' }}> <img src={sm} alt=""/> </p>
			</Col>
		</Row>
		<Row className="pt-0 pb-3">
			<Col className="ff-bold">
			    <p style={{ marginBottom: '0' }}>Shoes In Marikina</p>
			    <p style={{ marginBottom: '0' }}>Copyright ©2023 All Rights Reserved</p>
			</Col>
		</Row>
		</Container>
	</Container>

		</> 

	

	)
}