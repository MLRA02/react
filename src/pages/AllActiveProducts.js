import { useContext, useState, useEffect } from "react"
import UserContext from "../UserContext";
import {Navigate} from "react-router-dom";
import {Container, Col, Row, Button, Modal, Form, Card} from "react-bootstrap"
import Swal from "sweetalert2";
import Sidebar from "../components/SidebarMenu.js";
import React from "react";




export default function AllActiveProducts(){


    const { user } = useContext(UserContext);

    const [allProducts, setAllProducts]= useState([]);

    const [productName, setProductName] = useState("");
    const [productType, setProductType] = useState("");
    const [description, setDescription] = useState("");
    const [price, setPrice] = useState(0);
    const [productImage, setProductImage] = useState('')
    // const [stocks, setStocks] = useState(0);
    // const [imgSource, setImgSource] = useState("");

    const [isActive, setIsActive] = useState(false);

    const [modalAdd, setModalAdd] = useState(false);
    


    const addProductOpen = () => setModalAdd(true);
    const addProductClose = () => setModalAdd(false);

   



    /* GET ALL PRODUCTS */
	const fetchData = () =>{
		fetch(`${process.env.REACT_APP_API_URL}/products/active`, {
			headers:{
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

            setAllProducts(data.map(products =>{
                return (

                    <Container>
                    <Row key={products._id} >
                    <Col>
                    <Card>
                      <Row className="no-gutters">
                        <Col md={4}>
                          <Card.Img variant="top" src={`${process.env.REACT_APP_API_URL}/${products.productImage}`} />
                        </Col>
                        <Col md={8}>
                          <Card.Body>
                            <Card.Title>{products.productName}</Card.Title>
                            <Card.Subtitle>{products._id}</Card.Subtitle>
                            <Card.Title>Product Type:</Card.Title>
                            <Card.Subtitle>{products.productType}</Card.Subtitle>
                            <Card.Title>Product Description:</Card.Title>
                            <Card.Text>{products.description}</Card.Text>
                            <Card.Title>Price:</Card.Title>
                            <Card.Text>Php {products.price}</Card.Text>

                            <Card.Text>{products.isActive ? "Active" : "Inactive"}</Card.Text>

                            <Card.Text>
                                {(products.isActive)
                                ?
                                <Button variant="dark" size="sm" onClick={() => archive(products._id, products.name)}>Archive</Button>
                                :
                                <>
                
                                
                                </>}</Card.Text>
                          </Card.Body>
                        </Col>
                      </Row>
                    </Card>
                    </Col>
                    </Row>
                    </Container>
                    

                )
            }));
		});
	}


    /* ARCHIVE PRODUCT */
    const archive = (id, name) =>{
        console.log(id);
        console.log(name);
        fetch(`${process.env.REACT_APP_API_URL}/products/${id}/archive`,
        {
            method : "PUT",
            headers : {
                "Authorization": `Bearer ${localStorage.getItem("token")}`
            },
        body: JSON.stringify({
            isActive: false
        })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            if(data){
                Swal.fire({
                    title: "PRODUCT ARCHIVING SUCCESS!",
                    icon: "success",
                    text: `The product is now in archive`,
                    confirmButtonColor: "black"
                });

                fetchData();
            }else{
                Swal.fire({
                    title: "Archive Unsuccessful",
                    icon: "error",
                    text: "Something went wrong. Please try again later",
                    confirmButtonColor: "black"
                })
            }
        })
    }

    useEffect(()=>{
        fetchData();


    }, [])

   

	

    /* ADD PRODUCT */
    const addProduct = (e) => {
        e.preventDefault();

        const formData = new FormData()

        formData.append('productType', productType)
        formData.append('productName', productName)
        formData.append('description', description)
        formData.append('price', price)
        formData.append('productImage', productImage)

        fetch(`${process.env.REACT_APP_API_URL}/products/create`, {
            method: "POST",
            headers: {
                "Authorization": `Bearer ${localStorage.getItem('token')}`
            },
            body: formData
        })
            .then(res => {
                console.log(res);

                if (res) {
                    Swal.fire({
                        title: "PRODUCT ADDED SUCCESSFULLY!",
                        icon: "success",
                        text: `"The new product was added to the product list.`,
                        confirmButtonColor: "black"
                    });


                    fetchData();
                    addProductClose();
                }
                else {
                    Swal.fire({
                        title: "ADD PRODUCT UNSSUCCESSFUL!",
                        icon: "error",
                        text: `The system is experiencing trouble at the moment. Please try again later.`,
                        confirmButtonColor: "black"
                    });
                    addProductClose();
                }

            })
        setProductType('');
        setProductName('');
        // setImgSource('');
        setDescription('');
        setPrice(0);
        // setStocks(0);
    }

    

    useEffect(() => {

        if (productName !== "" && description !== "" && price > 0 ){     //&& stocks > 0) {
            setIsActive(true);
        } else {
            setIsActive(false);
        }

    }, [productName, description, price]); //, stocks




	return(
        <>
         
        <Container fluid className="w-100 d-flex h-100 m-0 p-0">
        <Row className="w-100 m-0 p-0">
        <Col className="bg-white d-flex  pt-3 m-0 shadow" xs={12} md={2} lg={2}>
        <Sidebar />
        </Col>
       
        <Col className="colr-bg m-0 p-5 d-flex justify-content-center" xs={12} md={10} lg={10}>
        {
            (user.isAdmin)
		?
        <>
		<div className="w-100 bg-white  rounded shadow-sm shadow-lg p-5">
			<div className="my-2 text-center">
				<h1 className="ff-bold pb-5">VIEW ALL PRODUCTS</h1>
			</div>
            <div className="my-2 text-right">
                    <Button  variant="dark" className="px-5" onClick={addProductOpen}>ADD A PRODUCT</Button>
            </div>
            <div>
                <Container>
                <Row className="all-products-row">
                {allProducts}
                </Row>
                </Container>
            </div>

             

                 {/* ADD PRODUCT MODAL */}

                <Modal
                size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                centered
                show={modalAdd}
                >
                <Form onSubmit={e => addProduct(e)} method="POST" encType='multipart/form-data'>

                        <Modal.Header className="text-light bg-dark">
                            <Modal.Title>ADD PRODUCT</Modal.Title>
                        </Modal.Header>

                        <Modal.Body>
                        <Row>
                        <Col xs={6} md={6} lg={6}>
{/*                        <Image className="editModal-img-fit mb-2"
                                src={imgSource}
                            />
*/}
                            <Form.Group controlId="productType" className="mb-3">
                                <Form.Label>Product Type</Form.Label>
                                <Form.Control as="select" value={productType} onChange={e => setProductType(e.target.value)} required>
                                    <option selected>Select product type</option>
                                    <option value="Basketball">Basketball</option>
                                    <option value="Sneaker">Sneaker</option>
                                    <option value="Running">Running</option>
                                </Form.Control>
                            </Form.Group>
                            <Form.Group controlId="ProductName" className="mb-3">
                                <Form.Label>Product Name</Form.Label>
                                <Form.Control
                                    type="text"
                                    placeholder="Product Name"
                                    value={productName}
                                    onChange={e => setProductName(e.target.value)}
                                    required
                                />
                            </Form.Group>
                            </Col>
                            <Col xs={6} md={6} lg={6}>
                            <Form.Group controlId="description" className="mb-3">
                                <Form.Label>Product Description</Form.Label>
                                <Form.Control
                                    as="textarea"
                                    rows={3}
                                    placeholder="Product Description"
                                    value={description}
                                    onChange={e => setDescription(e.target.value)}
                                    required
                                />
                            </Form.Group>

                            <Form.Group controlId="price" className="mb-3">
                                <Form.Label>Product Price</Form.Label>
                                <Form.Control
                                    type="number"
                                    placeholder="Product Price"
                                    value={price}
                                    onChange={e => setPrice(e.target.value)}
                                    required
                                />
                            </Form.Group>

<Form.Group controlId="fileName" className="mb-3">
                    <Form.Label>Upload Image</Form.Label>
                    <Form.Control
                        type="file"
                        name='image'
                        onChange={(e) => setProductImage(e.target.files[0])}
                        size="lg" />
                </Form.Group>
{/*                            <Form.Group controlId="imgSource" className="mb-3">
                                <Form.Label>Image Link</Form.Label>
                                <Form.Control
                                    type="text"
                                    placeholder="Product Image Link"
                                    value={imgSource}
                                    onChange={e => setImgSource(e.target.value)}
                                    required
                                />
                           
                            </Form.Group>*/}
                            </Col>
                            </Row>
                        </Modal.Body>

                        <Modal.Footer>
                            {isActive
                                ?
                                <Button variant="dark" type="submit" id="submitBtn">
                                    ADD PRODUCT
                                </Button>
                                :
                                <Button variant="light" type="submit" id="submitBtn" disabled>
                                    ADD PRODUCT
                                </Button>
                            }
                            <Button variant="dark" onClick={addProductClose}>
                                Close
                            </Button>
                        </Modal.Footer>

                    </Form>
                </Modal>
                
               


		</div>
        </>
		:
		<Navigate to="/" />}

        </Col>
        
        </Row>
    </Container>

    </>


	)
}
