import { useState, useEffect } from "react";
import {BrowserRouter as Router, Routes, Route} from "react-router-dom";
import AppNavbar from "./components/AppNavbar";
import Home from "./pages/Home";
import Products from "./pages/Products";
import Basketball from "./pages/Basketball";
import Sneaker from "./pages/Sneaker";
import Running from "./pages/Running";
import ProductView from "./pages/ProductView";
import Error from "./pages/Error";
import Login from "./pages/Login";
import Logout from "./pages/Logout";
import Register from "./pages/Register";
import AllProducts from "./pages/AllProducts";
import AllActiveProducts from "./pages/AllActiveProducts"
import AllOrders from "./pages/AllOrders.js";
import Cart from "./pages/Cart.js";
import AdminDashboard from "./pages/AdminDashboard";
import './App.css';
import { UserProvider } from "./UserContext";


function App() {

  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  });

  const unsetUser = () =>{
    localStorage.clear(); 
  }

  useEffect(()=>{
    console.log(user);
    console.log(localStorage);
  }, [user])
  useEffect(()=>{

    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      method: "POST",
      headers:{
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }
    })
    .then(res => res.json())
    .then(data => {
      console.log(data);

      if(data._id !== undefined){
        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        });
      }
      else{
        setUser({
          id: null,
          isAdmin: null
        });
      }
      
    })

  }, [])

  return (
    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>

        <AppNavbar />

          <Routes>
            <Route exact path="/" element={<Home />} />
            <Route exact path="/products" element={<Products />} />
            <Route exact path="/products/:productId" element={<ProductView />} />
            <Route exact path="/products/basketball" element={<Basketball />} />
            <Route exact path="/products/running" element={<Running />} />
            <Route exact path="/products/sneaker" element={<Sneaker />} />
            <Route exact path="/login" element={<Login />} />
            <Route exact path="/logout" element={<Logout />} />
            <Route exact path="/register" element={<Register />} />
            <Route exact path="/dashboard/orders" element={<AllOrders />} />
            <Route exact path="/dashboard" element={<AdminDashboard />} />
            <Route exact path="/dashboard/products" element={<AllProducts />} />
            <Route exact path="/dashboard/active" element={<AllActiveProducts />} />
            <Route exact path="/cart" element={<Cart />} />
            <Route exact path="*" element={<Error />} />
          </Routes>


      </Router>
    </UserProvider>
  );
}

export default App;
