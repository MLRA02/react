import React from "react"
import { Link } from "react-router-dom";
import { Container, Row, Col, Button } from "react-bootstrap";

export default function Hero(){


	return(

		<Container fluid className="hero-container">
			<Row className="hero-row align-items-center">
				<Col></Col>
				<Col></Col>
				<Col xs={12} lg={4} className="px-5">
					<Row >
					<h1 className="ff-bold">Shoes Made in Marikina</h1>
					</Row>
					<Row>
					<p className="ff-reg">SM is the premier destination for shoe enthusiasts in the area. Our store offers a wide variety of high-quality shoes at competitive prices. From running shoes to dress shoes, we have something for everyone. We are known for our extensive selection of locally made shoes from the renowned shoe-making city of Marikina. Our shoes are made with the finest materials and crafted by skilled artisans, ensuring both durability and style. In addition, our knowledgeable staff is always on hand to provide expert advice and assistance in finding the perfect pair of shoes for you. We also offer a wide range of shoe care products to help keep your shoes in top condition. </p>
					</Row>
					<Row className="pt-3">
					<Col xs={12} lg={6}>
						<Button  as={Link} to={"/products"} variant="dark" size="sm">SHOP NOW</Button>
					</Col>
					</Row>
				</Col>
			</Row>
		</Container>
	)
}
